# generator-typescript-basic
_Yeoman Generator, typescript-basic, for a Typescript module._



This generator creates an absolutely minimal Typescript app/module skeleton.



## Installation

First, install [Yeoman](http://yeoman.io) and [generator-typescript-basic](https://www.npmjs.com/package/generator-typescript-basic) using [npm](https://www.npmjs.com/).
(You'll obviously need [node.js](https://nodejs.org/) installed on your system. Bower is optional.).

```bash
npm install -g yo
npm install -g generator-typescript-basic
```

Then generate your new project:

```bash
mkdir my-ts-project
cd my-ts-project
yo typescript-basic
```


## License

MIT © [Harry Y](https://gitlab.com/yofactory)



